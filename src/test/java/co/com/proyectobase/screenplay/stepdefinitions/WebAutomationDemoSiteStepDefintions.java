package co.com.proyectobase.screenplay.stepdefinitions;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import co.com.proyectobase.screenplay.tasks.Registrar;
import co.com.proyectoreto1.screenplay.util.ExcelReader;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class WebAutomationDemoSiteStepDefintions {
	
	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor fernando = Actor.named("Fernando");

	@Before
	public void configuracionInicial(){
		fernando.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^that Fernando wants to use the Web Automation Demo Site$")
	public void thatFernandoWantsToUseTheWebAutomationDemoSite() throws Exception {
		fernando.wasAbleTo(Ingresar.ALaPaginaWebAutomationDemoSite());
	}

	@When("^he must register by completing the form$")
	public void heMustRegisterByCompletingTheForm() throws Exception {
		String Path = "src\\test\\resources\\datadriven\\dataDriven.xlsx";
		String SheetName = "Hoja1";
		ExcelReader.setExcelFile(Path, SheetName);
		fernando.attemptsTo(Registrar.UsuarioEnWebAutomationDemoSite());
		ExcelReader.CerrarBook();
	}

	@Then("^he verifies that the screen is loaded with text (.*)$")
	public void heVerifiesThatTheScreenIsLoadedWithTextDoubleClickOnEditIconToEDITTheTableRow(String tex) throws Exception {
		fernando.should(GivenWhenThen.seeThat(LaRespuesta.es(), Matchers.equalTo(tex)));
	}

}
