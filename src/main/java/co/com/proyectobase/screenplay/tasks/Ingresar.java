package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.WebAutomationDemoSite;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Ingresar implements Task{
	
	private WebAutomationDemoSite webAutomationDemoSite;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(webAutomationDemoSite));
	}

	public static Ingresar ALaPaginaWebAutomationDemoSite() {
		return Tasks.instrumented(Ingresar.class);
	}

}
