package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.WebAutomationDemoSite;
import co.com.proyectobase.screenplay.interactions.Select;
import co.com.proyectoreto1.screenplay.util.ExcelReader;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;


public class Registrar implements Task{

	@Override
	public <T extends Actor> void performAs(T actor) {
		try {
			String[] partsDate = ExcelReader.getCellData(1, 6).trim().split("-");
			actor.attemptsTo(
					Enter.theValue(ExcelReader.getCellData(1, 0).trim()).into(WebAutomationDemoSite.TEXT_FIRST_NAME),
					Enter.theValue(ExcelReader.getCellData(1, 1).trim()).into(WebAutomationDemoSite.TEXT_LAST_NAME),
					Enter.theValue(ExcelReader.getCellData(1, 2).trim()).into(WebAutomationDemoSite.TEXT_EMAIL),
					Enter.theValue(ExcelReader.getCellData(1, 3).trim()).into(WebAutomationDemoSite.TEXT_PHONE),
					Click.on(WebAutomationDemoSite.CHECK_GENDER),
					Select.ofTheList(WebAutomationDemoSite.SELECT_COUNTRIES, ExcelReader.getCellData(1, 5).trim()),
					Select.ofTheList(WebAutomationDemoSite.SELECT_YEAR, partsDate[0]),
					Select.ofTheList(WebAutomationDemoSite.SELECT_MONTH, partsDate[1]),
					Select.ofTheList(WebAutomationDemoSite.SELECT_DAY, partsDate[2]),
					Enter.theValue(ExcelReader.getCellData(1, 7).trim()).into(WebAutomationDemoSite.PASSWORD),
					Enter.theValue(ExcelReader.getCellData(1, 8).trim()).into(WebAutomationDemoSite.PASSWORD_CONFIRM),
					Click.on(WebAutomationDemoSite.BUTTON_SUMMIT)
					);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Performable UsuarioEnWebAutomationDemoSite() {
		return Tasks.instrumented(Registrar.class);
	}

}
