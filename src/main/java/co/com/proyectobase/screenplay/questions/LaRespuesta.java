package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.WebAutomationDemoSite;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaRespuesta implements Question<String> {

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(WebAutomationDemoSite.TEXT_CONFIRM).viewedBy(actor).asString();
	}

	public static LaRespuesta es() {
		return new LaRespuesta();
	}

}
