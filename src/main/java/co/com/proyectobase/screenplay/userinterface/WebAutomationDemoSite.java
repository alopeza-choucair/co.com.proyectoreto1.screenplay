package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class WebAutomationDemoSite extends PageObject{
	
	public static final Target TEXT_FIRST_NAME = Target.the("Campo de texto para el nombre").located(By.xpath("//INPUT[contains(@placeholder, \"First Name\")]"));
	public static final Target TEXT_LAST_NAME = Target.the("Campo de texto para el apellido").located(By.xpath("//INPUT[contains(@placeholder, \"Last Name\")]"));
	public static final Target TEXT_EMAIL = Target.the("Campo de texto para el email del usuario").located(By.xpath("//INPUT[contains(@type, \"email\")]"));
	public static final Target TEXT_PHONE = Target.the("Campo de texto para el telefono del usuario").located(By.xpath("//INPUT[contains(@type, \"tel\")]"));
	public static final Target CHECK_GENDER = Target.the("Check para identificar el sexo del usuario").located(By.xpath("//INPUT[contains(@value, \"Male\")]"));
	public static final Target SELECT_COUNTRIES = Target.the("Select para elegir el pais al que pertenece el usuario").located(By.id("countries"));
	public static final Target SELECT_YEAR = Target.the("Select para elegir el a�o de nacimiento").located(By.id("yearbox"));
	public static final Target SELECT_MONTH = Target.the("Select para elegir el mes de nacimiento").located(By.xpath("//SELECT[contains(@placeholder, \"Month\")]"));
	public static final Target SELECT_DAY = Target.the("Select para elegir el dia de nacimiento").located(By.id("daybox"));
	public static final Target PASSWORD = Target.the("Campo de texto para la contrase�a").located(By.id("firstpassword"));
	public static final Target PASSWORD_CONFIRM = Target.the("Campor de texto para la confirmacion de contrase�a").located(By.id("secondpassword"));
	public static final Target BUTTON_SUMMIT = Target.the("Boton para realizar la creacion del usuario").located(By.id("submitbtn"));
	public static final Target TEXT_CONFIRM = Target.the("Texto de verificaci�n").located(By.xpath("(//H4)[1]"));
}
